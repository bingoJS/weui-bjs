# WeUI-bjs 为微信 Web 服务量身设计  

git: [http://git.oschina.net/bingoJS/weui-bjs](http://git.oschina.net/bingoJS/weui-bjs) 

基于 [WeUI](https://github.com/weui/weui) 结合 [bingoJS 2.x](http://git.oschina.net/bingoJS/bingoJS2) 现实SPA（单页面应用）快速开发前端框架。

## 概述

WeUI 是一套同微信原生视觉体验一致的基础样式库，由微信官方设计团队为微信 Web 开发量身设计，可以令用户的使用感知更加统一。包含`button`、`cell`、`dialog`、 `progress`、 `toast`、`article`、`actionsheet`、`icon`等各式元素。结合bingoJS现实SPA（单页面应用）快速开发前端框架


## 手机预览

![http://bingojs.oschina.io/weui-bjs/dist/example/index.html](http://git.oschina.net/uploads/images/2016/0806/132847_d58e2a26_393548.png)

[http://bingojs.oschina.io/weui-bjs/dist/example/index.html](http://bingojs.oschina.io/weui-bjs/dist/example/index.html)

## 文档

WeUI 说明文档参考 [Wiki](https://github.com/weui/weui/wiki)

bingoJS 2.x 说明文档参考 [Wiki](http://git.oschina.net/bingoJS/bingoJS2/wikis/home)

## License
The MIT License(http://opensource.org/licenses/MIT)

请自由地享受和参与开源

## 贡献

如果你有好的意见或建议，欢迎给我们提issue或pull request