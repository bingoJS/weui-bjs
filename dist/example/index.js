
(function(bingo, $){
    "use strict";

    var app = bingo.app('example');

    app.controller('index', ['$view', '$site', function($view, $site){

        $view.$ready(function(){
            var url = App.hashUrl();
            url && $site.open(url);
        });

    }]); //end index

    app.controller('home', ['$view', '$site', function($view, $site) {


    }]); //end home

    app.controller('button', ['$view', '$site', function($view, $site) {


    }]); //end button


    app.controller('cell', ['$view', '$site', function($view, $site) {
      console.log('cell');
      $view.show = false;

      $view.$observe('show', function(c){
        if (c.value)
          setTimeout(function (){
                        $view.show = false;
                    }, 3000);
      });

    }]); //end cell


    app.controller('toast', ['$view', '$site', function($view, $site) {
      $view.showToast = false;
      $view.showLoadingToast = false;

      $view.$observe('showToast', function(c){
        if (c.value)
          setTimeout(function (){
                        $view.showToast = false;
                    }, 3000);
      });
      $view.$observe('showLoadingToast', function(c){
        if (c.value)
          setTimeout(function (){
                        $view.showLoadingToast = false;
                    }, 3000);
      });

    }]); //end toast

    app.controller('dialog', ['$view', '$site', function($view, $site) {
      $view.showDialog1 = false;
      $view.showDialog2 = false;

    }]); //end dialog

    app.controller('progress', ['$view', '$site', function($view, $site) {
      $view.progress = 0;
      $view.disabled = false;

      $view.start = function() {
        if ($view.disabled) return;
        $view.disabled = true;
        var progress = 0;
        var next = function() {
          progress = ++progress % 100;
          $view.progress = progress;
          setTimeout(next, 30);
        };

        next();
      };
    }]); //end progress

    app.controller('msg', ['$view', '$site', function($view, $site) {


    }]); //end msg

    app.controller('article', ['$view', '$site', function($view, $site) {


    }]); //end article

    app.controller('actionsheet', ['$view', '$site', function($view, $site) {
      $view.show = false;

    }]); //end actionsheet

    app.controller('icons', ['$view', '$site', function($view, $site) {


    }]); //end icons

    app.controller('panel', ['$view', '$site', function($view, $site) {


    }]); //end panel

    app.controller('tab', ['$view', '$site', function($view, $site) {


    }]); //end tab

    app.controller('navbar', ['$view', '$site', function($view, $site) {
      $view.index = 0;

    }]); //end navbar

    app.controller('tabbar', ['$view', '$site', function($view, $site) {
      $view.index = 0;

    }]); //end tabbar

    app.controller('searchbar', ['$view', '$site', function($view, $site) {

        $view.node = null;
        $view.$ready(function(){
          $($view.node).on('focus', '#search_input', function () {
                var $weuiSearchBar = $('#search_bar');
                $weuiSearchBar.addClass('weui_search_focusing');
            }).on('blur', '#search_input', function () {
                var $weuiSearchBar = $('#search_bar');
                $weuiSearchBar.removeClass('weui_search_focusing');
                if ($(this).val()) {
                    $('#search_text').hide();
                } else {
                    $('#search_text').show();
                }
            }).on('input', '#search_input', function () {
                var $searchShow = $("#search_show");
                if ($(this).val()) {
                    $searchShow.show();
                } else {
                    $searchShow.hide();
                }
            }).on('touchend', '#search_cancel', function () {
                $("#search_show").hide();
                $('#search_input').val('');
            }).on('touchend', '#search_clear', function () {
                $("#search_show").hide();
                $('#search_input').val('');
            });          
        });


    }]); //end searchbar


})(bingoV2, window.Zepto);

(function($) {
  'use strict';

   var app = bingo.app('example');

  var _hashReg = /#([^#]*)$/,
    _hashUrlReg = /^\s*np\s*\=\s*(.*?)\s*$/,
    _opening = false, _pageList = [],
    _checkPage = function(url){
      var list = url.split('/'),
        urlList = [],
        curList = [],
        pageList = [];
      bingo.each(list, function(item){
        curList.push(item);
        var tUrl = curList.join('/');
        urlList.push(tUrl);
      });
      bingo.each(_pageList, function(item){
        if (bingo.inArray(function(url){ return item.url == url; }, urlList) >= 0){
          pageList.push(item);
          $(item.cp.$getNodes()).hide();
        }
        else
          item.cp.$remove();
      });
      _pageList = pageList;
      return list[list.length -1];
    },
    _app = window.App = {
      hash: function(url) {
        url || (url = location + '');
        return _hashReg.test(url) ? RegExp.$1 : '';
      },
      hashUrl :function(url){
        var hash = _app.hash(url);
        return _hashUrlReg.test(hash) ? RegExp.$1 : ''
      },
      mainView:function(){
        return app.view('main');
      },
      mainCP:function(){
        return this.mainView().cp1;
      },
      open:function(url, ctrl){
        try{
          _opening = true;
          location.hash = 'np='+url;
          var cp = _app.mainCP();
          var view = _app.mainView();
          var id = _checkPage(url);
          if (id)
            $(view.inc1.$getNodes()).hide();

          return app.tmpl('#'+id).then(function(html){
            return cp.$insertAfter(html, null, ctrl).then(function(cp){
              _pageList.push({id:id, url:url,  cp:cp});
            });
          });
        } finally{
          _opening = false;
        }
      }
    };

  $(function() {
 
    $(window).on('hashchange', function() {
      if (_opening == true) return;
      var url =_app.hashUrl();
      if (url) {
        _app.open(url);
      } else{
        var view = _app.mainView();
        $(view.inc1.$getNodes()).show();
        _checkPage('');
      }
    });

  });

})(window.Zepto);
